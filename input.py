from ast import parse
import csv
import argparse
from types import NoneType
from setuptools import errors
from importlib.resources import path
from more_itertools import strip

parser = argparse.ArgumentParser(description="Pomoc dla programu Best Travel", add_help=False)
parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS, help='Wyświetla menu pomocy')
parser.add_argument("-c", "--cities", type=str, metavar="", help="Maksymalana liczba miast (liczba całkowita)")
parser.add_argument("-d", "--distance", type=str, metavar="", help="Maksymalana odległość (liczba całkowita)")
parser.add_argument("-p", "--path", type=str, metavar="", help="Ścieżka do pliku .csv (np. C:\Pulpit\\nazwa_pliku.csv)")
args = parser.parse_args()

no_value = False
wrong_value = False
wrong_path = False

try:
    if type(args.path) == NoneType:
        raise

except:
    print("-p, --path - Nie podano wartości")
    no_value = True




try:
    if type(args.cities) == NoneType:
        raise

    test = int(args.cities)


except ValueError:
    print("-c, --cities - Podana wartość nie jest liczbą całkowitą")
    wrong_value = True


except:
    print("-c, --cities - Nie podano wartości")
    no_value = True




try:
    if type(args.distance) == NoneType:
        raise

    test = int(args.distance)

except ValueError:
    print("-d, --distance - Podana wartość nie jest liczbą całkowitą")
    wrong_value = True

except:
    print("-d, --distance - Nie podano wartości")
    no_value = True




try:
    file = open(args.path)
    
except:
    if type(args.path) != NoneType:
        print("Podana ścieżka lub plik nie istnieje")
        wrong_path = True
    
    
    
if no_value == True or wrong_value == True or wrong_path == True:
    exit()
    



# declaration of variables for further use
path = args.path
distance = int(args.distance)
cities = int(args.cities)




    
csvreader = csv.reader(file)


values_string = ""


lines_of_input = 0  # checks how many lines there are in .csv file

# only firs line of .csv file is taken as input and converted to list
try:
    for value in file:

        lines_of_input += 1

        if lines_of_input > 1:
            raise ValueError

        values_string = value
                

except ValueError:
    print("UWAGA - Podany plik .csv posiada więcej niż jedną listę (linijkę) danych - Pod uwagę brana jest TYLKO pierwsza linijka pliku")

 
    
file.close()

# converting input from .csv file to list
striped_values_string = values_string.strip("\n")
values_string = striped_values_string
striped_values_string = values_string.replace(" ", "")

xs = striped_values_string.split(",")
    
position = 0

try:
    for element in xs:
        xs[position] = int(element)
        position += 1
    
except:
    print("Błąd - Plik nie zawiera nieodpowiednie dane lub jest pusty")
    exit()
