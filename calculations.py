import itertools

def best_sum(distance, cities, xs):
    
    sum_of_distances = 0

    for comb in itertools.combinations(xs, cities):

        if sum(comb) <= distance and sum(comb) > sum_of_distances:
            
            sum_of_distances = sum(comb)

    if sum_of_distances > 0:
        print("\nNajwiększa odległość spełniająca wymagania to: " + str(sum_of_distances))
    else:
        print("\nŻadna suma odległości nie spełnia podanych wymagań")
