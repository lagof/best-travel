# Readme for program Best Travel
The program calculates maximal sum of distances between given number of cities not exceeding given distance limit

## How to use:
Notice - To use program you need .cvs file with integer values separated with comas which are distances between cities

### For Windows
Type following commands in cmd or powershell :
cd [directory where program is located]
python executable.py [flags]

### For Linux
Type following commands in terminal:
cd [directory where program is located]
python3 executable.py [flags]

### List of flags
-h, --help - shows help menu
-p, --path - path to .csv file containing distances between cities
-d, --distance - maximal sum of distances between cities
-c, --cities - maximal number of cities

Notice - All flags have to be used once for program to run properly and not throw any errors